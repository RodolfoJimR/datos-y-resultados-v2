let cardServicios = document.getElementById("servicios");
let cardConocenos = document.getElementById("conocenos");
let cardTestimonial = document.getElementById("testimonial-1");
let cardCasos = document.getElementById("casos");
let cardContacto = document.getElementById("contacto");

var funcionBajar = () => {
    let y = window.scrollY;
    if(y >= 550){
        cardServicios.style.opacity = 1;
    }else{
        cardServicios.style.opacity = 0;
    }
    if( y >= 1300){
        cardConocenos.classList.add("mostrar-izquierda");
    }else{
        cardConocenos.classList.remove("mostrar-izquierda");
    }
    if( y >= 1700){
        cardTestimonial.classList.add("mostrar-derecha");
    }else{
        cardTestimonial.classList.remove("mostrar-derecha");
    }
    if(y >= 2500){
        cardCasos.classList.add("mostrar-arriba");
    }else{
        cardCasos.classList.remove("mostrar-arriba");
    }
    if(y >= 5000){
        cardContacto.classList.add("opacidad-visible");
    }else{
        cardContacto.classList.remove("opacidad-visible");
    }
};

window.addEventListener("scroll", funcionBajar);