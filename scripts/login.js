var OpenLogin = document.getElementById('btn-open-login'),
    overlay = document.getElementById('overlay'),
    login = document.getElementById('login'),
    CloseLogin = document.getElementById('btn-close-login');

OpenLogin.addEventListener('click', function(){
  overlay.classList.add('active');
  login.classList.add('active');
});

CloseLogin.addEventListener('click', function(e){
    e.preventDefault();
    overlay.classList.remove('active');
    login.classList.remove('active');
  });