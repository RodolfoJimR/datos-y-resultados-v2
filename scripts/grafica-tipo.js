var firebaseConfig = {
    apiKey: "AIzaSyCYMUiaE3ZzHSnZc2NYeShPFF1PNlT4uUs",
    authDomain: "dynamic-forms-87996.firebaseapp.com",
    databaseURL: "https://dynamic-forms-87996.firebaseio.com",
    projectId: "dynamic-forms-87996",
    storageBucket: "dynamic-forms-87996.appspot.com",
    messagingSenderId: "831911899167",
    appId: "1:831911899167:web:7ea7b4ce9ba558b6a0a52a",
    measurementId: "G-Z1NL5W726N"
  };

// Initialize Firebase
if (firebase.apps.length === 0) {
    firebase.initializeApp(firebaseConfig);
  }
firebase.analytics();

// Obtener id de usuario
firebase.auth().onAuthStateChanged((user) => {
    if (user) {
        var uid = user.uid;
        infoUsuarioGrafica(uid);
    } else {
    }
});
// Obtener número de formulario, crear botón, div y canvas dependiendo cuantos formularios existen
function infoUsuarioGrafica(uid){
    let flexBtn = document.getElementById("graphic-btn-flex");
    database.ref(`Usuarios/${uid}/Formularios/`).once("value", function(snapshot){
        snapshot.forEach(function(childSnapshot){
            let coleccionValue;
            let data = childSnapshot.key;
            let buttonChart = document.createElement("button");
            buttonChart.value = data;
            buttonChart.textContent = data;
            flexBtn.appendChild(buttonChart);
        
            buttonChart.addEventListener("click",function(){
                coleccionValue = buttonChart.value;
                mostrarGrafica(coleccionValue);
            });
        });
        
    });
}

function mostrarGrafica(coleccionValue){
    let graphicChart = document.getElementById("graphic-chart");
    let chartDiv = document.createElement("div");
    chartDiv.className = `myChart ${coleccionValue}`;
    let chartCanvas = document.createElement("canvas");
    chartCanvas.id = `myChart ${coleccionValue}`;
    
    chartDiv.appendChild(chartCanvas);
    graphicChart.appendChild(chartDiv);
    database.ref(`Usuarios/${id}/Formularios/${coleccionValue}/preguntas/`).once("value", function(snapshot){
        let abiertaCorta = 0;
        let abiertaLarga = 0;
        let opcionMultiple = 0;
        let multipleSeleccion = 0;
        snapshot.forEach(function(childSnapshot){
            let tipoPregunta = childSnapshot.val().tipo;
            if(tipoPregunta === 0){
                abiertaCorta++;
            } else if(tipoPregunta === 1){
                abiertaLarga++;
            } else if(tipoPregunta === 2){
                opcionMultiple++;
            } else if(tipoPregunta === 3){
                multipleSeleccion++;
            }
            
        });
        var ctx = document.getElementById(`myChart ${coleccionValue}`).getContext('2d');
        myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ['Abiertas (150 caracteres)', 'Abiertas (500 caracteres)', 'Opción múltiple', 'Múltiple selección'],
                datasets: [{
                    
                    data: [abiertaCorta, abiertaLarga, opcionMultiple, multipleSeleccion],
                    backgroundColor: [
                        '#334a8a',
                        '#d11f22',
                        '#d64e1c',
                        '#1ec92a'
                    ],
                    borderColor: [
                        '#111f45',
                        '#850f11',
                        '#9c3611',
                        '#15a11f'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                plugins: {
                    title: {
                        display: true,
                        text: `Tipos de pregunta de formulario ${coleccionValue}`
                    }
                }
            }
        });
    });
}